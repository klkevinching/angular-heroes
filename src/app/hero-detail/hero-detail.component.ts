import { Component, OnInit, Input } from '@angular/core';
import { Hero } from '../hero';

// The HeroDetailComponent needs a new way to obtain the hero-to-display.
// a. Get the route that created it,
// b. Extract the id from the route
// c. Acquire the hero with that id from the server via the HeroService
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HeroService }  from '../hero.service';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})

export class HeroDetailComponent implements OnInit {

  @Input() hero: Hero;

  goBack(): void {
    this.location.back();
  }

  getHero(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.heroService.getHero(id)
      .subscribe(hero => this.hero = hero);
  }

  constructor(

    // Inject the ActivatedRoute, HeroService, and Location services into the constructor, saving their values in private fields
    private route: ActivatedRoute,
    private heroService: HeroService,
    private location: Location

  ) { }

  ngOnInit(): void {
    this.getHero();
  }

}
