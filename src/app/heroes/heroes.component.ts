import { Component, OnInit } from '@angular/core';

// Open the HeroesComponent class file and import the mock HEROES.(replaced by Hero Service)
// import { HEROES } from '../mock-heroes';
import { HeroService } from '../hero.service';

import { Hero } from '../hero';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  // Define a component property called heroes to expose HEROES array for binding. (replaced by Hero Service)
  // heroes = HEROES;

  // Definition of the heroes property (for Hero Service)
  heroes: Hero[];

  // This will not work in a real app. You're getting away with it now because the service currently returns mock heroes. But soon the app will fetch heroes from a remote server, which is an inherently asynchronous operation.
  // The HeroService must wait for the server to respond, getHeroes() cannot return immediately with hero data, and the browser will not block while the service waits.
  // HeroService.getHeroes() must have an asynchronous signature of some kind.
  // (Replaced by Observable Hero Service)
  // getHeroes(): void {
  //   this.heroes = this.heroService.getHeroes();
  // }  

  // The new version waits for the Observable to emit the array of heroes— which could happen now or several minutes from now. Then subscribe passes the emitted array to the callback, which sets the component's heroes property.
  // This asynchronous approach will work when the HeroService requests heroes from the server.
  getHeroes(): void {
    this.heroService.getHeroes()
        .subscribe(heroes => this.heroes = heroes);
  }

  // Add the following onSelect() method, which assigns the clicked hero from the template to the component's selectedHero. (No Longer Use After adding Route)
  // selectedHero: Hero;
  // onSelect(hero: Hero): void {
  //   this.selectedHero = hero;
  // }

  constructor(private heroService: HeroService) { }

  ngOnInit() {
    this.getHeroes();
  }

}
